package threads;
public class Task implements Runnable
{
	int id;
	Friend person;
	String bf;

	Task(int i, Friend m, String bestFriend) {
		id = i;
		person = m;
		bf = bestFriend;
	}

	public void run() {
		System.out.printf("Trabalhando na tarefa %d\n", id);
		person.setBestTaskFriend(bf);
		System.out.printf(" -> Finalizando trabalho de %d, BF original: %s, BF final: %s\n", 
			id, bf,  person.getBestTaskFriend(id)); 
	}
}
