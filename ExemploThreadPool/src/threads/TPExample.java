package threads;
import java.util.concurrent.*;

public class TPExample
{
	public static void main(String[] args) {
	
		ExecutorService pool = Executors.newCachedThreadPool(); // banco de ilimitado
		Friend person = new Friend("Mr. X");

		for (int i = 0; i < 10; i++) {
			pool.execute(new Task(i, person, 
				String.format("Mr. %d", i)));
		}

		pool.shutdown();
	}
}
