package threads;
import java.util.concurrent.*;
import java.util.*;

public class Friend
{   
    String name;
    List<String> bestFriend;

    Friend(String n) {
        bestFriend = new ArrayList<String>();
        setName(n);
    }

    void setName(String n) {
        name = n;
    }
    void setBestTaskFriend(String i) {
        bestFriend.add(i);
    }
    String getBestTaskFriend(int i) {
        return bestFriend.get(i);
    }
}